# Facebook Recruiting IV: Human or Robot?

Dans le cadre du processus de recrutement chez Adomik, il m'a été proposé de faire un test Kaggle. Il s'agit de la compétition de recrutement “_Facebook Recruiting IV: Human or Robot?_”, disponible à [cette adresse](https://www.kaggle.com/c/facebook-recruiting-iv-human-or-bot). Le présent fichier essaye de retracer mes pas et mon processus de pensée dans la proposition d'une solution au problème.

Notons, toute fois, que le challenge original a duré environ un mois et demi. Je ne prétends donc pas que la solution décrite ici (faite en une journée) rivalise avec celles au top du leaderboard de la compétition (d'ailleurs, là n'est pas le but de l'exercice).

## Introduction (très rapide) du problème

Un site d'enchères veut détecter et éliminer les bots qui impactent négativement l'expérience utilisateur. Pour ce faire, deux jeux de données sont mis à disposition:

- Le premier jeu de données labélisé, qui contient des informations basiques sur les enchérisseurs (c.à.d. utilisateurs), à savoir l'identifiant, l'adresse et le moyen de payement (ces deux derniers étant obfusqués) en plus de l'information sur le fait qu'il s'agisse d'un bot ou non.
- Le deuxième jeu de données (plus riche) contient l'historique des enchères placées sur les différentes mises en vente (_auctions_) par les différents enchérisseurs.

Il est clair qu'il s'agit d'un problème de classification binaire où l'on veut déterminer le label Bot/Humain (plus précisément, on est intéressés par la probabilité qu'il s'agit d'un bot). Avant même de charger les données et de les regarder, mes intuitions sont les suivantes:

- Il y aura très probablement un problème de classes déséquilibrées. Si les bots étaient aussi nombreux que les humains, le site aurait été complétement déserté par ses utilisateurs.
- L'essentiel du travail portera sur le _feature engineering_ pour trouver des descripteurs pertinents permettant d'identifier le comportement suspicieux des bots.
- Le comportement temporel serait un facteur clé pour identifier les bots.

## Choisir mon "poison": R ou Python

D'habitude, je suis agnostique quand il s'agit de programmer: je choisis le langage soit sur la base de ce que je veux faire avec et/ou de ce qui existe chez mon "client". Chacun entre R et Python a ses avantages et bien sûr ses inconvénients.

Depuis quelques temps, je fais essentiellement du Python et j'arrête pas de vanter les mérites de scikit-learn quand il s'agit d'automatiser et de faire des pipelines de ses traitements de données. Par contre, je trouve que la gestion des types de variables (entre autre création de _dummy variables_) un peu mal faite, comparé à du R où c'est souvent transparent.

Je vois ce genre de tests comme une opportunité d'apprendre quelque chose de nouveau. J'avoue que jusqu'à avant mon switch à Python, je faisais beaucoup de choses, entre autres gérer la partie validation croisée et sélection de modèles, manuellement (j'en ai honte). C'est pour cela que j'ai trouvé que c'est l'occasion de jouer un peu avec le package `caret` de R. Mon choix du langage (R) est essentiellement dû à cette raison.

À noter que j'ai écarté Spark comme choix viable pour la simple raison que j'allais tourner le code en local sur ma pauvre machine personnelle.

## Premier contact avec les données

J'ai commencé par charger mes données et m'assurer que tout va bien (à base de `summary`).

```{R}
train <- read.csv("./train.csv") # infos de base sur les enchérisseurs
bids <- read.csv("./bids.csv") # historique des enchères
```

Un `summary` de l'historique des enchères montre par exemple que, à mon grand étonnement, les données sont assez propres: il n'y a pas de données manquantes ou des problèmes d'orthographe sur les données textuelles (comme `merchandise`, `country` et `device`). Seul point noir de ce tableau: l'obfuscation du temps (un point sur lequel je reviendrai plus tard).

```{R}
summary(bids)
```

```
bid_id                                        bidder_id          auction       
Min.   :      0   f5b2bbad20d1d7ded3ed960393bec0f40u6hn: 515033   jqx39  : 537347  
1st Qu.:1914083   197e90906939267ace2422e78e8e83889znqp: 236413   jefix  : 208926  
Median :3828166   a58ace8b671a7531c88814bc86b2a34cf0crb: 192565   no958  :  74020  
Mean   :3828166   13b022681839a351f07d017c0edd9ed6ao2w5: 168995   opnq4  :  71446  
3rd Qu.:5742250   9655ccc7c0c193f1549475f02c54dce45kjw7: 161935   du967  :  69633  
Max.   :7656333   29c103503e1d6ace5626fb07a5ab0dafrtjvs: 138576   enf1x  :  60626  
              (Other)                              :6242817   (Other):6634336  
      merchandise           device             time              country       
mobile          :2126587   phone4  : 706573   Min.   :9.632e+15   in     :1788731  
jewelry         :1902058   phone2  : 171584   1st Qu.:9.641e+15   ng     : 586586  
sporting goods  :1855207   phone35 : 165230   Median :9.701e+15   id     : 453611  
home goods      :1224234   phone65 : 153855   Mean   :9.698e+15   tr     : 319574  
office equipment: 289838   phone3  : 153202   3rd Qu.:9.762e+15   us     : 318103  
furniture       :  99181   phone101: 152048   Max.   :9.773e+15   za     : 297942  
(Other)         : 159229   (Other) :6153842                       (Other):3891787  
          ip                       url         
37.95.134.218  :  61203   vasstdc27m7nks3:3499409  
224.130.156.216:  53544   7zyltxp0hh36vpp:  22297  
13.31.35.207   :  47666   z3qzkki3dy6ndjb:  17909  
119.88.125.198 :  27033   q0skvht51258k93:  12350  
155.184.236.45 :  26302   vwjvx8n5d6yjwlj:  10446  
105.144.126.10 :  24180   xosquuqcro853d7:   9167  
(Other)        :7416406   (Other)        :4084756
```

Un petit `table` permet de vérifier qu'on est bien dans la présence de classes déséquilibrées (_unbalanced data_):

```{R}
table(train$outcome)
```

```
0    1
1910  103
```
![Class distribution](/images/class_dist_barplot.png)

C'est un point qu'il faut adresser en procédant par exemple au sur-échantillonnage de la classe minoritaire, un sous-échantillonnage de la classe majoritaire (vu la taille du dataset je pense qu'il faut éviter), etc. À noter que la métrique utilisée pour l'évaluation lors de la compétition (la ROC) tient compte de cet aspect (contrairement à une métrique telle que la _accuracy_).

Une petite vérification permet aussi de découvrir que 29 enchérisseurs dans le jeu d'apprentissage n'ont jamais placé d'enchères et qu'ils sont tous considérés comme humains.

```{R}
table(train$outcome[!(train$bidder_id %in% bids$bidder_id)])
```
```
0
29
```

C'est un constat intéressant qui m'a poussé à faire l'hypothèse de la “présomption d'innocence” lors de l'étiquettage du jeu de test: toute personne n'ayant pas placé au moins une enchère est considérée humaine sauf preuve du contraire.

Un autre constat que je mentionne rapidement: il parait que tronquer les variables `payment_account` et `address` en supprimant les 5 derniers caractères réduit les modalités de celles-ci mais toute suppression de caractères additionnels après ce point ne semble avoir aucun impact. C'est une information que je n'ai pas exploitée (au fait, je n'ai pas exploité ces deux variables tout court) mais peut être qu'il y a une piste à creuser par là.


## Feature engineering
Cette partie risque d'être la plus compliquée à décrire étant donné que je vais essayer de condenser le processus “itératif” d'essayer d'inventer des features qui puissent aider mes modèles à bien classer les enchérisseurs.

La fonction que j'ai écrite pour extraire des descripteurs des enchérisseurs à partir de leur historique d'enchères est la suivante:

```{R}
# function for extracting freatures from bids
extract_features <- function(bids){
  features <- bids %>% group_by(bidder_id) %>%
    summarise(n_auctions = n_distinct(auction), # total number of auctions
              n_bids = n_distinct(bid_id), # total number of bids
              n_unique_ips = n_distinct(ip), # number of unique IPs
              n_unique_countries = n_distinct(country), # number of unique countries
              n_unique_devices = n_distinct(device), # number of unique devices
              n_unique_urls = n_distinct(url), # number of unique urls
              outbid_timedelta_mean = mean(outbid_timedelta), # average outbid time
              outbid_timedelta_sd = sd(outbid_timedelta), # standard deviation of outbid time
              outbid_timedelta_max = max(outbid_timedelta), # max outbid time
              outbid_timedelta_min = min(outbid_timedelta) # min outbid time
    )

  features$never_outbade <- is.na(features$outbid_timedelta_mean) # boolean indicating if the bidder never outbade
  features$outbid_timedelta_mean[features$never_outbade] <- -9999 # for NA handling purposes
  features$outbid_timedelta_min[features$never_outbade] <- -9999 # for NA handling purposes
  features$outbid_timedelta_max[features$never_outbade] <- -9999 # for NA handling purposes
  features$outbid_timedelta_sd[features$never_outbade] <- -9999 # for NA handling purposes
  features$outbid_timedelta_sd[is.na(features$outbid_timedelta_sd)] <- 0 # by choice: if only one observation, SD = 0 instead of being undefined

  auction_features <- bids %>% group_by(bidder_id, auction) %>%
    summarise(n_bids = n_distinct(bid_id), n_urls = n_distinct(url), n_devices = n_distinct(device), n_countries = n_distinct(country)) %>%
    group_by(bidder_id) %>%
    summarise(n_bids_per_auction_mean = mean(n_bids), n_bids_per_auction_sd =sd(n_bids), # mean and standard deviation of the number of bids per auction
              n_urls_per_auction_mean = mean(n_urls), n_urls_per_auction_sd = sd(n_urls), # mean and standard deviation of the number of unique urls per auction
              n_countries_per_auction_mean = mean(n_countries), n_countries_per_auction_sd = sd(n_countries), # mean and standard deviation of the number of countries per auction
              n_devices_per_auction_mean = mean(n_devices), n_devices_per_auction_sd = sd(n_devices)) # mean and standard deviation of the number of devices per auction
  auction_features$n_bids_per_auction_sd[is.na(auction_features$n_bids_per_auction_sd)] <- 0 # same "convention for standard deviation: if one unique observation -> SD = 0 and not NA
  auction_features$n_urls_per_auction_sd[is.na(auction_features$n_urls_per_auction_sd)] <- 0 # same
  auction_features$n_countries_per_auction_sd[is.na(auction_features$n_countries_per_auction_sd)] <- 0 # same
  auction_features$n_devices_per_auction_sd[is.na(auction_features$n_devices_per_auction_sd)] <- 0 # same
  features <- plyr::join(features, auction_features, by = c('bidder_id'))

  # auction "winners" (how much auctions each bidder won)
  auction_winners <- bids %>% group_by(auction) %>%
    summarise(bidder_id = last(bidder_id))%>%
    group_by(bidder_id) %>%
    summarise(n_last_auctioner = n())

  features <- plyr::join(features, auction_winners, by = c('bidder_id'))
  features$n_last_auctioner[is.na(features$n_last_auctioner)] <- 0
  features$last_auctioner_to_auction_ratio <- features$n_last_auctioner / features$n_auctions # also make ratio

  features
}
```

En gros, mes raisonnements et les variables qui en résultaient sont les suivants:

- J'ai commencé par créer des variables qui modélisent le comportement “global” de chaque enchérisseur. Ainsi, on a le nombre total de mises en vente auxquelles il a participé (`n_auctions`), nombre total d'enchères placées (`n_bids`), le nombre d'appareils uniques (`n_unique_devices`), le nombre d'IPs uniques (`n_unique_ips`), le nombre d'URLs uniques (`n_unique_urls`) et le nombre de pays uniques (`n_unique_countries`). Intuitivement, je m'attends à ce qu'il y ait plus de “variabilité” pour certaines de ces variables dans le cas des bots que dans le cas des humains. Par exemple, je m'attends à ce que la majorité des humains utilisent un nombre “raisonnable” d'appareils (ou IPs) comparés à des bots.
- Je me suis également dit qu'il fallait s'intéressait au comportement (et surtout sa variabilité) à une échelle plus “micro”. Autrement dit, on peut se poser la question: est-ce que le comportement de l'utilisateur est similaire d'une mise en vente à une autre (il utilise toujours le même nombre d'appareils, place un nombre semblable d'enchères, etc.) ou non? Je me suis dit que la clé d'identifier les bots est peut être là! Chaque comportement entrant sous cette réflexion est modélisé par une mesure de centralité (la moyenne, suffixe `_mean`) et une mesure de dispersion (l'écart type, suffixe `_sd`).
- Bien évidemment, il faut aussi modéliser le comportement temporel de l'enchérisseur. Je me suis dit qu'il faut s'intéresser au temps qu'il met d'habitude pour sur-enchérir (intuitivement je m'attendrais à ce qu'un bot réagisse très vite pour sur-enchérir comparé à un humain). Pour modéliser cette réflexion, j'avoue que j'y suis allé un peu en “overkill” (en vain) puisque j'ai gardé le temps minimum, moyen + écart type et max pour sur-enchérir de chaque utilisateur (`outbid_timedelta_min`, `outbid_timedelta_mean`, `outbid_timedelta_sd`, et `outbid_timedelta_max` respectivement). J'ai également réalisé qu'il y a des utilisateurs qui n'ont jamais sur-enchéri du coup j'ai décidé d'ajouter une variable (booléenne, `never_outbade`) qui le signale.
- Enfin, en lisant et relisant la phrase “_Human bidders on the site are becoming increasingly frustrated with their inability to win auctions vs. their software-controlled counterparts_” dans l'énoncé de la compétiton, je me suis dit qu'il faut aussi modéliser combien de mises en vente chaque enchérisseur a gagné. Ainsi, je garde le nombre de mises en vente emportées (`n_last_auctioner`) et le ratio qu'elles constituent des mises en vente auxquelles l'utilisateur a participé (`last_auctioner_to_auction_ratio`).

Les variables que j'ai créées sont donc les suivantes:

- `n_auctions`: nombre total de mises en vente auxquelles l'enchérisseur a participé.
- `n_bids`: nombre total d'enchères placées.
- `n_unique_ips`: nombre total d'IPs uniques utilisées.
- `n_unique_urls`: nombre total d'URLs uniques utilisées.
- `n_unique_devices`: nombre total d'appareils utilisés.
- `n_unique_countries`: nombre total de pays associés à l'enchérisseur.
- `n_bids_per_auction_mean`: nombre moyen d'enchères placées par mise en vente à laquelle l'enchérisseur a participé.
- `n_bids_per_auction_sd`: écart type du nombre d'enchères par mise en vente à laquelle l'enchérisseur a participé.
- `n_urls_per_auction_mean`: nombre moyen d'URLs utilisées par mise en vente à laquelle l'enchérisseur a participé.
- `n_urls_per_auction_sd`: écart type du nombre d'URLs utilisées par mise en vente à laquelle l'enchérisseur a participé.
- `n_devices_per_auction_mean`: nombre moyen d'appareils utilisés par mise en vente à laquelle l'enchérisseur a participé.
- `n_devices_per_auction_sd`: écart type du nombre d'appareils utilisés par mise en vente à laquelle l'enchérisseur a participé.
- `n_countries_per_auction_mean`: nombre moyen de pays par mise en vente à laquelle l'enchérisseur a participé.
- `n_countries_per_auction_sd`: écart type du nombre de pays par mise en vente à laquelle l'enchérisseur a participé.
- `outbid_timedelta_min`: laps de temps minimum pour sur-enchérir.
- `outbid_timedelta_mean`: laps de temps moyen pour sur-enchérir.
- `outbid_timedelta_sd`: écart type du laps de temps pour sur-enchérir.
- `outbid_timedelta_max`: laps de temps maximum pour sur-enchérir.
- `never_outbade`: si oui ou non l'enchérisseur n'a jamais sur-enchéri.
- `n_last_auctioner`: nombre de fois où l'enchérisseur était le dernier à placer une enchère (i.e. a gagné la mise en vente)
- `last_auctioner_to_auction_ratio`: ratio entre le nombre de de mises en vente remportées et les mises en vente totales auxquelles l'enchérisseur a participé.

Avant de passer à la section suivante, je tiens à faire quelques remarques:

- Dans une situation "normale", je prends le temps de bien mener une étude exploratoire et de vérifier la pertinence des variables que je définis. Notamment, moyennant des visualisations (type barplot, scatter-plot, etc.) j'essaye de voir s'il y a des différences de distributions entre les classes (cf. le boxplot montrant la différence de la distribution du nombre d'IPs uniques entre bots et humains), si certaines variables sont corrélées ou présentent des colinéarités, etc. Malheureusement par faute de temps, j'en ai pas eu l'occasion ici.
![IP Distribution for humans and bots](/images/unique_ip_dist_boxplot.png)
- Certaines variables que j'ai crées en cours de route n'ont pas été retenues à la fin (comme le pays le plus fréquent), notamment parce qu'elles n'apportaient pas grand chose dans les premiers modèles que j'ai testés. Aussi, à un certain moment j'ai essayé de calculer des indicateurs de type “entropie” pour caractériser l'utilisation des appareils, IPs, etc. mais c'était très onéreux en tant de calcul et ça n'apportait pas grand chose.

## Sélection de modèles

Passons maintenant à la construction de nos modèles de classification. Dans les situations où je dois "pondre" un modèle ASAP, j'ai tendance à privilégier les modèles appartenant à la famille “_ensemble learning_”. Notamment, mes deux “go to models” sont les _random forests_ et les _gradient boosted decision trees_ (notamment pour leur robustesse face au phénomène d'overfitting). J'ai également considéré pour cet exercice un SVM linéaire et un SVM avec noyau radial.

La sélection du meilleur modèle de chacune des quatre familles (Random Forest, GBM, SVM, SVMr) a été effectuée avec une validation croisée avec 10 sous-échantillons (i.e. _10 fold cross-validation_) tout en sur-échantillonnant la classe minoritaire. La ROC a été utilisée comme critère de qualité. J'ai défini la grille d'hyper-paramètres à chaque fois en utilisant des valeurs usuelles (à noter que le package `caret` ne permet pas directement de faire varier le nombre d'arbres de décision dans le random forest).

```{R}
# fit control for all models: use 10-fold cross validation, up-sample minority class
fitControl <- trainControl(method = "cv",
                           number = 10,
                           classProbs = TRUE,
                           sampling = 'up',
                           summaryFunction = twoClassSummary)

# Random Forest model
rf_grid <- expand.grid(mtry = 1:5)
rf_fit <- caret::train(outcome ~ ., data = train_features[, -1], method = 'rf', trControl = fitControl, metric = "ROC", tuneGrid = rf_grid, verbose = FALSE)

# Gradient boosted decision trees
gbm_grid <- expand.grid(n.trees = c(50, 100, 250, 500),
                        interaction.depth = 1:5,
                        shrinkage = c(0.001, 0.01, 0.1),
                        n.minobsinnode = c(10, 25, 50))
gbm_fit <- caret::train(outcome ~ ., data = train_features[, -1], method = 'gbm', trControl = fitControl, metric = "ROC", tuneGrid = gbm_grid, verbose = FALSE)

# linear SVM
svm_grid <- expand.grid(C = c(0.001, 0.01, 0.1, 1, 100, 1000))
svm_fit <- caret::train(outcome ~ ., data = train_features[, -1], method = 'svmLinear', trControl = fitControl, metric = "ROC", tuneGrid = svm_grid, verbose = FALSE)

# SVM with radial kernel
svmr_grid <- expand.grid(C = c(0.001, 0.01, 0.1, 1, 10, 100, 1000), sigma = c(0.001, 0.01, 0.1, 1, 100, 1000))
svmr_fit <- caret::train(outcome ~ ., data = train_features[, -1], method = 'svmRadial', trControl = fitControl, metric = "ROC", tuneGrid = svmr_grid, verbose = FALSE)
```

Grâce au package `caret`, il est facile de comparer les modèles entre eux:

```{R}
resamps <- resamples(list(RF = rf_fit,
                          GBM = gbm_fit,
                          SVM = svm_fit,
                          SVMr = svmr_fit))

summary(resamps)
```

```
Models: RF, GBM, SVM, SVMr
Number of resamples: 10

ROC
       Min. 1st Qu. Median   Mean 3rd Qu.   Max. NA's
RF   0.8269  0.8640 0.8699 0.8883  0.9246 0.9598    0
GBM  0.8213  0.8921 0.9128 0.9062  0.9370 0.9516    0
SVM  0.7452  0.8076 0.8281 0.8359  0.8625 0.9165    0
SVMr 0.6912  0.8014 0.8686 0.8467  0.8984 0.9218    0

Sens
       Min. 1st Qu. Median   Mean 3rd Qu.   Max. NA's
RF   0.1000  0.1250 0.2500 0.2591  0.3636 0.5000    0
GBM  0.6364  0.7455 0.8500 0.8173  0.9000 0.9091    0
SVM  0.4545  0.6250 0.7636 0.7100  0.8000 0.9000    0
SVMr 0.3636  0.5250 0.6500 0.6018  0.7000 0.7273    0

Spec
       Min. 1st Qu. Median   Mean 3rd Qu.   Max. NA's
RF   0.9681  0.9840 0.9867 0.9846  0.9894 0.9894    0
GBM  0.8032  0.8258 0.8351 0.8341  0.8411 0.8617    0
SVM  0.7447  0.7713 0.7798 0.7804  0.7926 0.8138    0
SVMr 0.8457  0.8790 0.8856 0.8883  0.8989 0.9259    0
```

```{R}
bwplot(resamps, layout = c(3, 1))
```

![BWPplot](/images/bwp.png)

(note à soi: voir pourquoi le random forest se casse tellement la gueule sur la sensitivity)

```{R}
dotplot(resamps, metric = "ROC")
```

![ROC](/images/ROC.png)

Il est clair que c'est le modèle GBM qui est supérieur (c'est celui qui sera donc retenu comme modèle final). Le meilleurs paramètres pour celui-ci peuvent être obtenus par l'instruction suivante

```{R}
gbm_fit$bestTune
```

qui indique qu'il est composé de 100 arbres où chaque arbre a une profondeur égale à 4 et où les nœuds terminaux contiennent au moins 25 observations. Le régularisation de celui-ci est effectuée avec un paramètre de shrinkage de 0.01.

```
n.trees interaction.depth shrinkage n.minobsinnode
    100                 4      0.01             25
```

## Classification du jeu de test

La classification des observations dans le jeu de test est assez simple: tous les enchérisseurs qui n'ont pas effectué d'enchères sont d'abord étiquettés comme étant des humains. Le modèle est utilisé pour déterminer la classe de ceux qui restent.

```{R}
presumed_innocent <- data.frame(bidder_id = test$bidder_id[!(test$bidder_id %in% bids$bidder_id)], prediction = 0.0)
test_features <- features_from_bids[features_from_bids$bidder_id %in% test$bidder_id,]
test_predictions <- data.frame(bidder_id = test_features$bidder_id, prediction = predict(svm_fit, test_features[, -1], type = 'prob')$bot)
test_submission <- rbind(test_predictions, presumed_innocent)
```
## Améliorations possibles

Il y a plusieurs pistes qui peuvent être explorées pour améliorer les résultats obtenus ici. En voici quelques unes qui me semblent prometteuses:

- __Décortiquer l'obfuscation qui a été effectuée sur le temps__: à mon avis c'est LA chose la plus importante à faire si l'on veut générer de bonnes features temporelles. Par faute de temps, j'ai pas regardé trop en détail la variable `time` dans l'historique des enchères. L'histogramme décrivant la distribution des valeurs de cette variable montre trois “paquets” distincts de valeurs. Cette observation suggère qu'il y a trois transformations différentes qui étaient appliquées au temps. À mon avis si l'on arrive à bien deviner la nature des transformations et faire en sorte de rendre les valeurs de la variable comparables sur tout le jeu de données, on arrivera à définir des features temporels plus pertinents.
![Time distribution](/images/time-hist.png)
- __Définir d'autres variables temporelles__: dans la continuité du point précédent, on peut imaginer d'autres variables telles que: (i) le temps moyen écoulé entre deux enchères successives de l'enchérisseur, (ii) le temps écoulé entre la dernière enchère de l'enchérisseur et la fin de la mise en vente, etc.
- __Créer des variables “collectives”__: la quasi-totalité des variables que j'ai définies (à l'exception du temps écoulé avant de sur-enchérir) sont calculées individuellement (i.e., n'utilisent que les enchères de l'individu en question). Il serait probablement intéressant de créer des variables qui inspectent plusieurs individus à la fois. Par exemple, le nombre d'IPs utilisées par l'enchérisseur qui ont déjà été utilisées par des bots.
- __Considérer d'autres modèles__: si j'avais plus de temps, je testerai bien d'autres modèles (des réseaux de neurones peut être?) que ceux qui étaient considérés ici.
