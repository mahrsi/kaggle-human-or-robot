# The code makes the assumptions that:
# 1. All the needed packages are already installed.
# 2. The csv files are located in the current working directory.

# load packages
library(caret)
library(plyr)
library(dplyr)

SEED <- 3784

# load files
train <- read.csv("./train.csv")
test <- read.csv("./test.csv")
bids <- read.csv("./bids.csv")

# order bids chronologically, per auction
bids <- bids[with(bids, order(auction, time)),]

# create a variable modelling how much time elapses between current bid and previous bid on same auction
bids$outbid_timedelta <- c(NA, diff(bids$time))
bids$outbid_timedelta[c(TRUE, (bids$auction[-1] != bids$auction[-(nrow(bids))]))] <- NA

# function for extracting freatures from bids
extract_features <- function(bids){
  features <- bids %>% group_by(bidder_id) %>%
    summarise(n_auctions = n_distinct(auction), # total number of auctions
              n_bids = n_distinct(bid_id), # total number of bids
              n_unique_ips = n_distinct(ip), # number of unique IPs
              n_unique_countries = n_distinct(country), # number of unique countries
              n_unique_devices = n_distinct(device), # number of unique devices
              n_unique_urls = n_distinct(url), # number of unique urls
              outbid_timedelta_mean = mean(outbid_timedelta), # average outbid time
              outbid_timedelta_sd = sd(outbid_timedelta), # standard deviation of outbid time
              outbid_timedelta_max = max(outbid_timedelta), # max outbid time
              outbid_timedelta_min = min(outbid_timedelta) # min outbid time
    )
  
  features$never_outbade <- is.na(features$outbid_timedelta_mean) # boolean indicating if the bidder never outbade
  features$outbid_timedelta_mean[features$never_outbade] <- -9999 # for NA handling purposes
  features$outbid_timedelta_min[features$never_outbade] <- -9999 # for NA handling purposes
  features$outbid_timedelta_max[features$never_outbade] <- -9999 # for NA handling purposes
  features$outbid_timedelta_sd[features$never_outbade] <- -9999 # for NA handling purposes
  features$outbid_timedelta_sd[is.na(features$outbid_timedelta_sd)] <- 0 # by choice: if only one observation, SD = 0 instead of being undefined
  
  auction_features <- bids %>% group_by(bidder_id, auction) %>%
    summarise(n_bids = n_distinct(bid_id), n_urls = n_distinct(url), n_devices = n_distinct(device), n_countries = n_distinct(country)) %>%
    group_by(bidder_id) %>%
    summarise(n_bids_per_auction_mean = mean(n_bids), n_bids_per_auction_sd =sd(n_bids), # mean and standard deviation of the number of bids per auction
              n_urls_per_auction_mean = mean(n_urls), n_urls_per_auction_sd = sd(n_urls), # mean and standard deviation of the number of unique urls per auction
              n_countries_per_auction_mean = mean(n_countries), n_countries_per_auction_sd = sd(n_countries), # mean and standard deviation of the number of countries per auction
              n_devices_per_auction_mean = mean(n_devices), n_devices_per_auction_sd = sd(n_devices)) # mean and standard deviation of the number of devices per auction
  auction_features$n_bids_per_auction_sd[is.na(auction_features$n_bids_per_auction_sd)] <- 0 # same "convention for standard deviation: if one unique observation -> SD = 0 and not NA
  auction_features$n_urls_per_auction_sd[is.na(auction_features$n_urls_per_auction_sd)] <- 0 # same
  auction_features$n_countries_per_auction_sd[is.na(auction_features$n_countries_per_auction_sd)] <- 0 # same
  auction_features$n_devices_per_auction_sd[is.na(auction_features$n_devices_per_auction_sd)] <- 0 # same
  features <- plyr::join(features, auction_features, by = c('bidder_id'))
  
  # auction "winners" (how much each auctions each bidder won)
  auction_winners <- bids %>% group_by(auction) %>%
    summarise(bidder_id = last(bidder_id))%>%
    group_by(bidder_id) %>%
    summarise(n_last_auctioner = n())
  
  features <- plyr::join(features, auction_winners, by = c('bidder_id'))
  features$n_last_auctioner[is.na(features$n_last_auctioner)] <- 0
  features$last_auctioner_to_auction_ratio <- features$n_last_auctioner / features$n_auctions # also make ratio
  
  features
}

# extract features from bids
features_from_bids <- extract_features(bids)
# choose those of the train dataset
train_features <- features_from_bids[features_from_bids$bidder_id %in% train$bidder_id,]
# add class
train_features$outcome <- c('human', 'bot')[train$outcome[match(train_features$bidder_id, train$bidder_id)] + 1] 

#### MODEL SELECTION ####

# fit control for all models: use 10-fold cross validation, up-sample minority class
fitControl <- trainControl(method = "cv",
                           number = 10,
                           classProbs = TRUE,
                           sampling = 'up',
                           summaryFunction = twoClassSummary)

# Random Forest model
rf_grid <- expand.grid(mtry = 1:5)
set.seed(SEED)
rf_fit <- caret::train(outcome ~ ., data = train_features[, -1], method = 'rf', trControl = fitControl, metric = "ROC", tuneGrid = rf_grid, verbose = FALSE)

# Gradient boosted decision trees
gbm_grid <- expand.grid(n.trees = c(50, 100, 250, 500),
                        interaction.depth = 1:5,
                        shrinkage = c(0.001, 0.01, 0.1),
                        n.minobsinnode = c(10, 25, 50))
set.seed(SEED)
gbm_fit <- caret::train(outcome ~ ., data = train_features[, -1], method = 'gbm', trControl = fitControl, metric = "ROC", tuneGrid = gbm_grid, verbose = FALSE)

# linear SVM
svm_grid <- expand.grid(C = c(0.001, 0.01, 0.1, 1, 100, 1000))
set.seed(SEED)
svm_fit <- caret::train(outcome ~ ., data = train_features[, -1], method = 'svmLinear', trControl = fitControl, metric = "ROC", tuneGrid = svm_grid, verbose = FALSE)

# SVM with radial kernel
svmr_grid <- expand.grid(C = c(0.001, 0.01, 0.1, 1, 10, 100, 1000), sigma = c(0.001, 0.01, 0.1, 1, 10, 100, 1000))
set.seed(SEED)
svmr_fit <- caret::train(outcome ~ ., data = train_features[, -1], method = 'svmRadial', trControl = fitControl, metric = "ROC", tuneGrid = svmr_grid, verbose = FALSE)

# Model comparison (made easy thanks to caret)
resamps <- resamples(list(RF = rf_fit,
                          GBM = gbm_fit,
                          SVM = svm_fit,
                          SVMr = svmr_fit))

summary(resamps)
bwplot(resamps, layout = c(3, 1))
dotplot(resamps, metric = "ROC")

# Labelling the test set
presumed_innocent <- data.frame(bidder_id = test$bidder_id[!(test$bidder_id %in% bids$bidder_id)], prediction = 0.0)
test_features <- features_from_bids[features_from_bids$bidder_id %in% test$bidder_id,]
test_predictions <- data.frame(bidder_id = test_features$bidder_id, prediction = predict(svm_fit, test_features[, -1], type = 'prob')$bot)
test_submission <- rbind(test_predictions, presumed_innocent)
write.table(test_submission, "submission.txt", row.names = F, sep =',', quote = F)
